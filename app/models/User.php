<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $primaryKey = 'sid';

	public $timestamps = false;
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * The field list for mass assignment.
	 *
	 * @var array
	 */
    protected $fillable = array('domain_id','created_dt','updated_dt','updated_counter','uuid','original_domain_id','id','password','first_name','middle_name','last_name','lc_name','display_name','bdate','phone','email','permalink','activation_code','security_question','security_answer','login_fail_counter','status','default_lang_cd','timezone','expiry_dt','active_project_sid','type','params_text');

    // Validation
    private static $validation_rules = array(
    	'id' => 'required|alpha_num|min:6',
    	'password' => 'required|alpha_num|min:6',
    	'first_name' => 'required|alpha|min:2',
    	'email' => 'required|email|unique:users'
    	);

    /**
     * @Tutorial
     * Validation
     */
    public static function validator($fields)
    {
    	$validator = Validator::make($fields, static::$validation_rules);

    	return $validator;
    }

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

}