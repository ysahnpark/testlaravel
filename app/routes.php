<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

header('Access-Control-Allow-Origin: *');
Route::get('/hello', function()
{
	return View::make('hello');
});

/**
 * @Tutorial
 */
Route::get('/test', 'TestController@showHelloWorld');
Route::get('/testfancy', 'TestController@showFancyHello');

// The resource Controler contains a predefined set of methods
Route::resource('users', 'UserController');

// The resource Controler contains a predefined set of methods
Route::resource('api/users', 'UserApiController');

// With this routing, the controller contains methods where the action
// name is prefixed by HTTP method, eg. getXx
Route::controller('auth', 'AuthController');