
<!-- app/views/users/index.blade.php -->
@section('content')
<div class="container">


<h1>Users</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>Sid</td>
			<td>Domain_id</td>
			<td>Created_dt</td>
			<td>Updated_dt</td>
			<td>Id</td>
			<td>Name</td>
			<td>Lc_name</td>
			<td>Display_name</td>
			<td>Email</td>
			<td>Status</td>
			<td>Timezone</td>
			<td>Expiry_dt</td>
			<td>Type</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
	@foreach($list as $key => $value)
		<tr>
			<td>{{ $value->sid }}</td>
			<td>{{ $value->domain_id }}</td>
			<td>{{ $value->created_dt }}</td>
			<td>{{ $value->updated_dt }}</td>
			<td>{{ $value->id }}</td>
			<td>{{ $value->first_name }} {{ $value->middle_name }} {{ $value->last_name }}</td>
			<td>{{ $value->lc_name }}</td>
			<td>{{ $value->display_name }}</td>
			<td>{{ $value->email }}</td>
			<td>{{ $value->status }}</td>
			<td>{{ $value->timezone }}</td>
			<td>{{ $value->expiry_dt }}</td>
			<td>{{ $value->type }}</td>

			<!-- we will also add show, edit, and delete buttons -->
			<td>

				<!-- show the record (uses the show method found at GET /nerds/{id} -->
				<a class="btn btn-small btn-success" href="{{ URL::to('users/' . $value->sid) }}">Show</a>

				<!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('users/' . $value->sid . '/edit') }}">Edit</a>

				<!-- delete the record (uses the destroy method DESTROY /nerds/{id} -->
				{{ Form::open(array('url' => 'users/' . $value->sid, 'class' => 'pull-right')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}

			</td>
		</tr>
	@endforeach
	</tbody>
</table>

</div> <!-- container -->
@show