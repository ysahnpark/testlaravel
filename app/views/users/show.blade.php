
<!-- app/views/users/show.blade.php -->

@section('content')
<div class="container">

<!-- @todo - the field to be displayed as title -->

<h1>View {{ $record->sid }}</h1>

<dl class="dl-horizontal">
	<dt>{{ Lang::get('users.domain_id') }}</dt>
    <dd>{{ $record->domain_id }}</dd>
	<dt>{{ Lang::get('users.created_dt') }}</dt>
    <dd>{{ $record->created_dt }}</dd>
	<dt>{{ Lang::get('users.updated_dt') }}</dt>
    <dd>{{ $record->updated_dt }}</dd>
	<dt>{{ Lang::get('users.updated_counter') }}</dt>
    <dd>{{ $record->updated_counter }}</dd>
	<dt>{{ Lang::get('users.uuid') }}</dt>
    <dd>{{ $record->uuid }}</dd>
	<dt>{{ Lang::get('users.original_domain_id') }}</dt>
    <dd>{{ $record->original_domain_id }}</dd>
	<dt>{{ Lang::get('users.id') }}</dt>
    <dd>{{ $record->id }}</dd>
	<dt>{{ Lang::get('users.password') }}</dt>
    <dd>{{ $record->password }}</dd>
	<dt>{{ Lang::get('users.first_name') }}</dt>
    <dd>{{ $record->first_name }}</dd>
	<dt>{{ Lang::get('users.middle_name') }}</dt>
    <dd>{{ $record->middle_name }}</dd>
	<dt>{{ Lang::get('users.last_name') }}</dt>
    <dd>{{ $record->last_name }}</dd>
	<dt>{{ Lang::get('users.lc_name') }}</dt>
    <dd>{{ $record->lc_name }}</dd>
	<dt>{{ Lang::get('users.display_name') }}</dt>
    <dd>{{ $record->display_name }}</dd>
	<dt>{{ Lang::get('users.bdate') }}</dt>
    <dd>{{ $record->bdate }}</dd>
	<dt>{{ Lang::get('users.phone') }}</dt>
    <dd>{{ $record->phone }}</dd>
	<dt>{{ Lang::get('users.email') }}</dt>
    <dd>{{ $record->email }}</dd>
	<dt>{{ Lang::get('users.permalink') }}</dt>
    <dd>{{ $record->permalink }}</dd>
	<dt>{{ Lang::get('users.activation_code') }}</dt>
    <dd>{{ $record->activation_code }}</dd>
	<dt>{{ Lang::get('users.security_question') }}</dt>
    <dd>{{ $record->security_question }}</dd>
	<dt>{{ Lang::get('users.security_answer') }}</dt>
    <dd>{{ $record->security_answer }}</dd>
	<dt>{{ Lang::get('users.login_fail_counter') }}</dt>
    <dd>{{ $record->login_fail_counter }}</dd>
	<dt>{{ Lang::get('users.status') }}</dt>
    <dd>{{ $record->status }}</dd>
	<dt>{{ Lang::get('users.default_lang_cd') }}</dt>
    <dd>{{ $record->default_lang_cd }}</dd>
	<dt>{{ Lang::get('users.timezone') }}</dt>
    <dd>{{ $record->timezone }}</dd>
	<dt>{{ Lang::get('users.expiry_dt') }}</dt>
    <dd>{{ $record->expiry_dt }}</dd>
	<dt>{{ Lang::get('users.active_project_sid') }}</dt>
    <dd>{{ $record->active_project_sid }}</dd>
	<dt>{{ Lang::get('users.type') }}</dt>
    <dd>{{ $record->type }}</dd>
	<dt>{{ Lang::get('users.params_text') }}</dt>
    <dd>{{ $record->params_text }}</dd>
</dl>

</div> <!-- container -->
@show