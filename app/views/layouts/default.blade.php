<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test Laravel 4</title>
	{{ HTML::style('bootstrap/css/bootstrap.css') }}
	{{ HTML::script('bootstrap/js/bootstrap.js') }}
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);
	</style>
</head>
<body>

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('users') }}">Laravel Test</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('users') }}">View All Users</a></li>
		<li><a href="{{ URL::to('users/create') }}">Create a User</a></li>
	</ul>
	<ul class="nav navbar-nav pull-right">
		@if(!Auth::check())
           <li>{{ HTML::link('users/create', 'Register') }}</li>   
           <li>{{ HTML::link('auth/signin', 'Login') }}</li>   
        @else
           <li>{{ HTML::link('auth/signout', 'Logout') }}</li>
        @endif
	</ul>
	</ul>
</nav>

	@yield('content')
</body>
</html>
