<?php


class UserController extends \BaseController {

    protected $layout = 'layouts.default';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $list = User::all();
        $this->layout->content = View::make('users.index')
            ->with('list', $list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->layout->content = View::make('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = Input::all();

        $validator = User::validator($data);
        if ($validator->passes()) {
            $record = new User();
            $record->fill($data);
            $now = new DateTime;
            $now_str = $now->format('Y-m-d H:i:s');
            $record->uuid = uniqid();
            $record->created_dt = $now_str;
            $record->updated_dt = $now_str;
            $record->updated_counter = 0;
            // The pasword has to be hashed, otherwise the Auth::attempt() will now work.
            $record->password = Hash::make(Input::get('password'));
            $record->save();

            Session::flash('message', 'Successfully created!');
            return Redirect::to('users');
        } else {
            return Redirect::to('users/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $record = User::find($id);

        // show the view and pass the nerd to it
        $this->layout->content = View::make('users.show')
            ->with('record', $record);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $record = User::find($id);
        $this->layout->content = View::make('users.edit')
            ->with('record', $record);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();
        
        $validator = User::validator($data);
        if (true) {
            $record = User::find($id);
            $record->fill($data);
            $record->password = Hash::make(Input::get('password'));
            $record->save();

            Session::flash('message', 'Successfully updated!');
            return Redirect::to('users');
        } else {
            return Redirect::to('users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $record = User::find($id);
        $record->delete();

        // redirect
        Session::flash('message', 'Successfully deleted!');
        return Redirect::to('users');
    }
}