# Introduction

The best way to work this hand-on tutorial is to 

1. fork the project (from repo server, e.g. github or bitbucket)
	If you are logged in to the git service, you will probably see a button "Fork." Go ahead, press that button!
2. clone it locally
    `git clone https://<git-account>@bitbucket.org/<git-account>/testlaravel.git` 
3. create a new branch
	`git branch checkout -b my-experiment`
4. Run the php server locally (instead of php 5.4+'s `php -S localhost:9000 server.php`) do `php artisan serve`
5. Change the code and see what happens.

Optionally: `git remote add upstream https://<youraccount>@bitbucket.org/ysahnpark/testlaravel.git`

This sample project includes the following tutorial-like sections

## [Hello World](tutorial/1_helloworld.md)
As all programming tutorial should start: A super-simplistic page that displays "Hello World".
It will define a route that is attached to a simple controller class.

## [The Artisan tool](tutorial/2_artisan.md)
Introduction to the command line interface (CLI) tool that facilitates the development of Laravel application.

## [View & blade template](tutorial/3_views.md)
Using blade template and layout to render the page.


## Routing
[http://laravel.com/docs/routing](http://laravel.com/docs/routing)
Focus on 

- Route Parameters with regex constraints: 
- Route Filters: Useful for checking access authorization 
- Named Routes: for resolving URL from names

## [Controllers and REST resources, and nested resources](tutorial/_controllers.md)
Implementing a resource controller mapping REST method semantics to the actions (GET, PUT, POST, DELETE).

Further reading: [http://laravel.com/docs/controllers](http://laravel.com/docs/controllers)

## [Database Configuration, Migration and Seeding](tutorial/_database.md)
Creating migration script that creates tables in the DBMS, and seeding it with data.

Further reading: [http://laravel.com/docs/database], [http://laravel.com/docs/migrations]

## Eloquent ORM and Models

Further reading:[http://laravel.com/docs/eloquent]

## Unit Testing

Further reading: [http://laravel.com/docs/testing]

## Sessions 

## Authentication

## Validation

## Localization