# Laravel's CLI: Artisan

Check the official [Laravel doc on Artisan](http://laravel.com/docs/artisan)

To create a migration file:
$php artisan migrate:make create_users_table
The resulting file is saved at app/database/migrations

To create resource controller:
php artisan controller:make PhotoController