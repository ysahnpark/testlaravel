# Installation

Easiest way is through composer, 

	composer create-project laravel/laravel <your-project-name> --prefer-dist

otherwise clone the Laravel project.

## Running the application

If you have php 5.4+ you can run the included web server: `php artisan serve`.
Note that if you do `php -S localhost:8000` and then try to access public assets from the browser (e.g. image file in the `/public` folder) you will get an error: "The requested resource was not found on this server"

(You can get the php version by `php -v`)

Otherwise, you can create clone on DocumentRoot directory (On mac is /Library/WebServer/Documents/) and change `public` and `app/storage` folders' group to _www `sudo chgrp -R _www public` 


### On Mac
If you have not enabled php in the httpd.conf, you will nee to do so first.

    $vi /etc/apache2/httpd.conf

And uncomment the line
    
    LoadModule php5_module libexec/apache2/libphp5.so

You will need to change the `AllowOverride` from `None` to `All`.

Now start (or restart) the apache server by

	$sudo apachectl start | restart 

Now you can go to DocumentRoot (by default it is `/Library/WebServer/Document`) folder and create the phpinfo.php file that constains a single php code line:

	<? phpinfo();

Open a browser and go to [http://localhost/phpinfo.php](http://localhost/phpinfo.php)

Now in that same folder, you can make a symbolic link to point to the Laravel test project.

    ln -s </path/to/testlaravel/> testlaravel

You will also possibly need to modify the php.ini (e.g. enable mysql, etc.)
Find out where the php is by 

    $php --ini

It is probably `/etc/php.ini`

YOu will need to install/enable mcrypt
