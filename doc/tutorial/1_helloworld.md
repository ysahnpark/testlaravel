# The first app: Hello World

## The router

Laravel understand how to thranslate the URL into execution of actual PHP code through the Routing.

For example if want the URL `http://mysite.com/user/` to open a page that shows the list of users, then you will need to setup Route so the `GET` method calls a specific PHP code.
A simple way to define a Route is using lamba (anonymous functions), but for our example we will use a controller class.

For the above example it would mean modifying the `app/routes.php` by adding a route line, something like: 

    Route::get('user', 'UserController@index');

For the Hello World we will create a HelloController in the app/controllers directory with the filename TestController.php as

    <?php
    class TestController extends BaseController { 

        public function showHelloWorld()
        {
            return 'Hello World';
        }
    }


Now, from the project root directory, run the php server:
`$php -S localhost:9000 server.php`

And open a browser to the location `http:\\localhost:9000/test`



Next: move onto [Artisan Usage](2_artisan.md)

Or check the [Laravel's documentation](http://laravel.com/docs/quick)