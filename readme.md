# Laravel 4 Test Project

## Setting up.
The best way to get started with Laravel project is to through [Composer](http://getcomposer.org/). If you have issue installing composer you should be able to just clone this project and go (assuming your php 5.x+ was correctly installed).

## Testing
Since PHP 5.4, the PHP installation comes with a php web server. You can run the web server by
  php -S localhost:9000
Use `php artisan serve` instead.
 

## About Laravel
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework)

## Laravel Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).
